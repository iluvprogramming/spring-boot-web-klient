package com.example.ovningsuppgifter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OvningsUppgifterApplication {

    public static void main(String[] args) {
        SpringApplication.run(OvningsUppgifterApplication.class, args);
    }

}
